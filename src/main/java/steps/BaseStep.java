package steps;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import other.TestProperties;
import pages.MainPage;

import java.util.concurrent.TimeUnit;

public abstract class BaseStep {
    static WebDriver driver;
    public static void initDriver(){
        try {
            ChromeOptions options = new ChromeOptions();
            //options.addArguments("--headless");
            options.addArguments("--window-size=1920,1080");
            //options.addArguments("test-type");
            options.addArguments("disable-popup-blocking");
            options.addArguments("--disable-extensions");
            //options.addArguments("incognito");
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
            driver = new ChromeDriver(options);
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
            driver.get((String) TestProperties.getInstance().get("base.url"));
        } catch(Exception x){initDriver();}
    }
    public static void closeDriver(){
        driver.close();
    }
/*    @Attachment (type = "image/png", value = "Screenshot")
    public static byte[] takeScreenshot() {
        return ((TakeScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }*/
}

//for example
/*@Step("Выбран пункт галвноего меню - {0}") действие = шаг
public void stepSelectMenuItem(String item){
    menuPage.selectMainMenu(item)
        }*/
