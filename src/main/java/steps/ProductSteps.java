package steps;

import io.qameta.allure.Step;
import pages.ProductPage;

import static steps.BaseStep.driver;

public class ProductSteps {
    ProductPage productPage;
    public ProductSteps (){
        this.productPage = new ProductPage(driver);
    }

    @Step
    public ProductSteps addProductInBasket(String text){
        productPage.chooseProductFromList(text);
        return this;
    }
    public void getTotal(){
        productPage.getTotalCost();
    }
}
