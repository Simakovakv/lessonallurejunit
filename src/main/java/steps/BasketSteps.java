package steps;

import io.qameta.allure.Step;
import pages.BasketPage;

public class BasketSteps extends BaseStep {
    BasketPage basketPage;
    public BasketSteps() {
        basketPage = new BasketPage(driver);
    }

    @Step ("Проверяем, совпадает ли количество добавленных продуктов в корзине")
    public BasketSteps checkProductExist(){
        basketPage.checkProductExist();
        return this;
    }
    @Step ("Проверяем, совпадает ли общая стоимость добавленных продуктов в корзине")
    public BasketSteps checkProductCost(){
        basketPage.checkProductCost();
        return this;
    }
    @Step ("Удаляем все продкты из корзины")
    public BasketSteps removeAllProductsFromBasket(){
        basketPage.removeItems();
        return this;
    }

}
