package steps;

import io.qameta.allure.Step;
import pages.MainPage;

public class MainSteps extends BaseStep{
    MainPage mainPage;
    public MainSteps (){
        mainPage = new MainPage(driver);
        mainPage.closePopUpWindow();
    }

   @Step("выбрана категория из меню \"{0}\"")
    public ProductSteps chooseFromDeliveryMenu(String text){
        mainPage.clickMenu()
                .chooseCategoryFromMenu(text);
        return new ProductSteps();
    }
    @Step("выполнен переход к корзине")
    public BasketSteps enterToBasketButton(){
        mainPage.viewBasket();
        return new BasketSteps();
    }
}
