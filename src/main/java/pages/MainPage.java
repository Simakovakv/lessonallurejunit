package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

import static org.junit.Assert.assertNotEquals;

public class MainPage extends BasePage{
    @FindBy(xpath="//*[@id='desktopMenuMain']//a[contains(text(), 'Меню доставки')]")
    WebElement menuButton;
/*
    @FindBy(xpath="//*[@id='desktopMenuMain]//*[@id='desktopMenuMain']//a[@href='/catalog']/..//div[@class='main-menu__wrap']/ul[contains(@class, 'main-menu__list')]")
    WebElement menuItems;*/
    @FindBy(xpath="//*[@class='pw-confirm__content']//*[contains(text(),'Thank you but no')]")
    WebElement popUpWindow;

    @FindBy(xpath="//a[@title = 'Перейти в корзину']")
    WebElement basketButton;

    private static final String menuFormat = "//*[@id='desktopMenuMain']//li[@class='level-2__item']/a[contains(text(),'%s')]";

    public MainPage(WebDriver driver){
        super(driver);
    }
    public MainPage clickMenu(){
        click(menuButton);
        return this;
    }
    public MainPage closePopUpWindow(){
        if(checkVisibility(popUpWindow))
            popUpWindow.click();
        return this;
    }

    public ProductPage chooseCategoryFromMenu(String itemName){
        WebElement href = chooseItemFromList(menuFormat, itemName);
        this.click(href);
        return new ProductPage(driver);
    }

    public BasketPage viewBasket(){
        click(basketButton);
        return new BasketPage(driver);
    }
    @Override
    public boolean isPageLoaded() {
        return false;
    }
}
