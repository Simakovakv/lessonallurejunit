package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.junit.Assert.assertEquals;


public class BasketPage extends BasePage {

    @FindBy(xpath="//table[@class='shop-table']//tbody//tr")
    List<WebElement> productList;

    @FindBy(id = "cartPrice")
    WebElement totalCostField;

    @FindBy(xpath="//*[@class='a-left remove-td']//a")
    WebElement removeButton;

    public BasketPage(WebDriver driver){
        super(driver);
    }
    @Override
    public boolean isPageLoaded() {
        return false;
    }
    public void checkProductExist(){
        if(!productList.isEmpty())
            waitOf(ExpectedConditions.visibilityOfAllElements(productList));
        assertEquals(String.format("Count of elements into basket is not equals count of added elements"), productList.size(), ProductPage.counter );
    }
    public void checkProductCost(){
        waitOf(ExpectedConditions.visibilityOf(totalCostField));
        assertEquals(String.format("Total cost of elements into basket is not equals total cost of added elements"), Integer.valueOf(totalCostField.getText()).intValue(), ProductPage.totalCost );
    }

    public void removeItems(){
        try {
            ProductPage.counter = 0;
            ProductPage.totalCost = 0;
            while (removeButton.isDisplayed()) {
                removeButton.click();
            }

        } catch(Exception e){}
    }


}
