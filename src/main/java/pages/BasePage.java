package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.function.Function;

import static org.junit.Assert.assertNotEquals;

public abstract class BasePage {
    WebDriver driver;
    public BasePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    protected WebDriver getDriver(){
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public abstract boolean isPageLoaded();

    public void click(WebElement element){
        waitOf(ExpectedConditions.elementToBeClickable(element));
        scrollTo(element);
        element.click();
    }
    public void scrollTo(WebElement element){
        waitOf(ExpectedConditions.visibilityOf(element));
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", element);
    }
    void waitOf(Function func){
        new WebDriverWait(driver, 5).until(func);
    }

    public boolean checkVisibility(WebElement element){
        if(element.isEnabled())
            return true;
      else {return false;}
    }

    public void selectByText(WebElement element, String text){
        new Select(element).selectByVisibleText(text);
    }

    public void fillInputField(WebElement element, String text){
        waitOf(ExpectedConditions.visibilityOf(element));
        element.clear();
        element.sendKeys(text);
    }
    public WebElement chooseItemFromList(String stringFormat, String itemName){
        By itemLocator = By.xpath(String.format(stringFormat, itemName));
        //List<WebElement> href = driver.findElements(By.xpath("//*[@id='desktopMenuMain']//li[@class='level-2__item']/a[contains(text(),'"+text+"')]"));
        waitOf(ExpectedConditions.visibilityOfAllElements(driver.findElements(itemLocator)));
        List<WebElement> href = driver.findElements(itemLocator);
        assertNotEquals(String.format("%s item in list is not found", itemName), href.size(), 0 );
        return href.get(0);
    }

}
