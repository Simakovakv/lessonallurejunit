package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductPage extends BasePage {
    public static int totalCost = 0;
    public static int counter = 0;
    public ProductPage(WebDriver driver){
        super(driver);
    }
    String productButtonFormat = "//*[contains(text(), '%s')]//ancestor::*[@class='product-info']//button";
    String productPriceFormat = "//*[contains(text(), '%s')]//ancestor::*[@class='product-info']//*[@class='price-box']";
    @FindBy(xpath="//*[@class='products-grid']//li")
    List<WebElement> productList;

    public ProductPage chooseProductFromList(String text){
        WebElement button = chooseItemFromList(productButtonFormat, text);
        totalCost += Integer.valueOf(chooseItemFromList(productPriceFormat, text).getText().replaceAll("\\D+", "")).intValue();
        this.click(button);
        counter++;
        return this;
    }
    public void getTotalCost(){
        System.out.println(totalCost);
    }
    @Override
    public boolean isPageLoaded() {
        return false;
    }
}
