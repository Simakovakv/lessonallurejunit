/**
 * 1. Перейти на chaihona.ru
 * 2. Перейти в меню доставки - шаурма.
 * 3. Выбрать шаурму с курицей, запомнить название и цену.
 * 4. Выбрать меню доставки - Плов - Плов чайханский - запомнить цену.
 * 5. Перейти в корзину
 * 6. Проверить, что все товары добавлены.
 * 7. Проверить, что итоговая сумма совпадает с суммой выбранных товаров.
 * 8. Удалить все товары из корзины.
 * 9. Проверить, что корзина пустая
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import steps.BaseStep;
import steps.MainSteps;

public class ChaihonaTest {
    @Before
    public void startTest(){
        BaseStep.initDriver();
    }
    @After
    public void endTest(){
        BaseStep.closeDriver();
    }
    @Test
    public void productBasketChaihonaTest(){
        MainSteps steps = new MainSteps();
        steps.chooseFromDeliveryMenu("Шаурма")
                .addProductInBasket("Шаурма с курицей");
        steps.chooseFromDeliveryMenu("Плов")
                .addProductInBasket("Плов Чайханский");
        steps.enterToBasketButton()
                .checkProductExist()
                .checkProductCost()
                .removeAllProductsFromBasket()
                .checkProductExist();
    }
}
